def Regular_Item(item):
    """Update the values of a regular item"""
    if item.sell_in <= 0:
        # Once the sell by date has passed, Quality degrades twice as fast
        item.quality = item.quality - 2 if item.quality >= 2 else 0
    else:
        item.quality = item.quality - 1 if item.quality >= 1 else 0
        item.sell_in = item.sell_in - 1 if item.sell_in >= 1 else 0


def Aged_Brie(item):
    """Update the values of a Aged Brie"""
    item.quality = item.quality + 1 if item.quality < 50 else 50
    item.sell_in = item.sell_in - 1 if item.sell_in >= 1 else 0


def Sulfuras(item):
    """Update the values of a Sulfuras"""
    item.quality = 80


def Backstage_Pass(item):
    """Update the values of a Backstage Pass"""
    if item.sell_in == 0:  # Quality drops to 0 after the concert
        item.quality = 0
    elif item.sell_in <= 5:
        item.quality += 3
    elif item.sell_in <= 10:
        item.quality += 2
    else:
        item.quality += 1
    if item.quality > 50:
        item.quality = 50
    item.sell_in = item.sell_in - 1 if item.sell_in > 1 else 0


def Conjured(item):
    """Update the values of a Conjured item"""
    if item.sell_in == 0:
        item.quality -= 4 if item.quality >= 4 else 0
    else:
        item.quality -= 2 if item.quality >= 2 else 0
    item.sell_in -= 1 if item.sell_in >= 1 else 0

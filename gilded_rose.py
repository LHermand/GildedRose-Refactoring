from item_updates import Regular_Item, Aged_Brie, Sulfuras, Backstage_Pass, Conjured


class GildedRose(object):
    """GildedRose object Class"""

    def __init__(self, items):
        self.items = items

    def update_quality_item(self, item):
        """Update the item quality according to the item type

        Args:
            item (Item) : Item to update
        """
        transition_dict = {
            "Aged Brie": Aged_Brie,
            "Sulfura": Sulfuras,
            "Backstage passes": Backstage_Pass,
            "Conjured": Conjured,
            "": Regular_Item,
        }
        for name in transition_dict:
            if name in item.name:
                transition_dict[name](item)
                break

    def update_quality(self):
        """Update the quality of the items after the day"""
        for item in self.items:
            self.update_quality_item(item)


class Item:
    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = quality

    def __repr__(self):
        return "%s, %s, %s" % (self.name, self.sell_in, self.quality)

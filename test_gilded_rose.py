# -*- coding: utf-8 -*-
import unittest

from gilded_rose import *


class GildedRoseTest(unittest.TestCase):
    def test_foo(self):
        items = [Item("foo", 0, 0)]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual("foo", items[0].name)

    def test_item(self):
        items = [
            Item("Item 1", 1, 1),
            Item("Item 2", 45, 25),
            Item("Item 3", 0, 25),
            Item("Item 4", 10, 0),
        ]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual(items[0].sell_in, 0)
        self.assertEqual(items[0].quality, 0)
        self.assertEqual(items[1].sell_in, 44)
        self.assertEqual(items[1].quality, 24)
        self.assertEqual(items[2].sell_in, 0)
        self.assertEqual(items[2].quality, 23)
        self.assertEqual(items[3].sell_in, 9)
        self.assertEqual(items[3].quality, 0)

    def test_aged_brie(self):
        items = [
            Item("Aged Brie 1", 10, 0),
            Item("Aged Brie 2", 10, 49),
            Item("Aged Brie 3", 10, 50),
            Item("Aged Brie 4", 10, 51),
        ]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual(items[0].quality, 1)
        self.assertEqual(items[0].sell_in, 9)
        self.assertEqual(items[1].quality, 50)
        self.assertEqual(items[2].quality, 50)
        self.assertEqual(items[3].quality, 50)

    def test_sulfuras(self):
        items = [Item("Sulfuras 1", 10, 0), Item("Sulfuras 2", 4, 49)]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual(items[0].quality, 80)
        self.assertEqual(items[1].quality, 80)

    def test_backstage_pass(self):
        items = [
            Item("Backstage passes 1", 10, 10),
            Item("Backstage passes 2", 7, 10),
            Item("Backstage passes 3", 5, 10),
            Item("Backstage passes 4", 1, 10),
            Item("Backstage passes 5", 0, 10),
        ]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual(items[0].quality, 12)
        self.assertEqual(items[0].sell_in, 9)
        self.assertEqual(items[1].quality, 12)
        self.assertEqual(items[1].sell_in, 6)
        self.assertEqual(items[2].quality, 13)
        self.assertEqual(items[3].quality, 13)
        self.assertEqual(items[4].quality, 0)

    def test_conjured(self):
        items = [Item("Conjured 1", 10, 10), Item("Conjured 2", 0, 10)]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual(items[0].quality, 8)
        self.assertEqual(items[0].sell_in, 9)
        self.assertEqual(items[1].quality, 6)

    def test_texttest(self):
        items = [
            Item(name="+5 Dexterity Vest", sell_in=10, quality=20),
            Item(name="Aged Brie", sell_in=2, quality=0),
            Item(name="Elixir of the Mongoose", sell_in=5, quality=7),
            Item(name="Sulfuras, Hand of Ragnaros", sell_in=0, quality=80),
            Item(name="Sulfuras, Hand of Ragnaros", sell_in=-1, quality=80),
            Item(
                name="Backstage passes to a TAFKAL80ETC concert", sell_in=15, quality=20
            ),
            Item(
                name="Backstage passes to a TAFKAL80ETC concert", sell_in=10, quality=49
            ),
            Item(
                name="Backstage passes to a TAFKAL80ETC concert", sell_in=5, quality=49
            ),
            Item(name="Conjured Mana Cake", sell_in=3, quality=6),  # <-- :O
        ]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEqual(items[0].sell_in, 9)
        self.assertEqual(items[0].quality, 19)
        self.assertEqual(items[1].sell_in, 1)
        self.assertEqual(items[1].quality, 1)
        self.assertEqual(items[2].sell_in, 4)
        self.assertEqual(items[2].quality, 6)
        self.assertEqual(items[3].sell_in, 0)
        self.assertEqual(items[3].quality, 80)
        self.assertEqual(items[4].sell_in, -1)
        self.assertEqual(items[4].quality, 80)
        self.assertEqual(items[5].sell_in, 14)
        self.assertEqual(items[5].quality, 21)
        self.assertEqual(items[6].sell_in, 9)
        self.assertEqual(items[6].quality, 50)
        self.assertEqual(items[7].sell_in, 4)
        self.assertEqual(items[7].quality, 50)
        self.assertEqual(items[8].sell_in, 2)
        self.assertEqual(items[8].quality, 4)


if __name__ == "__main__":
    unittest.main()

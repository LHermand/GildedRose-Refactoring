# GildedRose Refactoring

## Install the requirements

Open a terminal in the current folder. Then tape and launch the following line : 
"pip install -r requirements.txt"

## Launch the Program

In a terminal, tape and launch the following line :
"python3 texttest_fixture.py"

## Launch the tests

In a terminal, tape and launch the following line :
"pytest -v"